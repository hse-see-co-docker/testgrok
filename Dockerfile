FROM openshift/base-centos7
MAINTAINER https://gitlab.cern.ch/hse-see-co-docker/testgrok

EXPOSE 8080

ENV TOMCAT_VERSION=7.0.78 

LABEL io.k8s.description="Platform for running Java applications on Tomcat 7" \
      io.k8s.display-name="Tomcat 7.0.75 JRE7" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="builder,tomcat,tomcat7" 

# Install Maven, tomcat
RUN INSTALL_PKGS="tar unzip ctags bc which lsof java-1.7.0-openjdk java-1.7.0-openjdk-devel" && \
    yum install -y --enablerepo=centosplus $INSTALL_PKGS && \
    rpm -V $INSTALL_PKGS && \
    yum clean all -y && \
    mkdir -p /tomcat && \
    (curl -v http://mirror.easyname.ch/apache/tomcat/tomcat-7/v$TOMCAT_VERSION/bin/apache-tomcat-$TOMCAT_VERSION.tar.gz | tar -zx --strip-components=1 -C /tomcat)


    # apt-get install -y --no-install-recommends wget tar default-jre tomcat8 exuberant-ctags git  \

ENV OPENGROK_TOMCAT_BASE /tomcat

# #Ctags
# RUN wget "http://prdownloads.sourceforge.net/ctags/ctags-5.8-1.src.rpm" -O /opt/ctags-5.8-1.src.rpm
# RUN yum install -y /opt/ctags-5.8-1.src.rpm && yum clean all -y
# RUN rm /opt/ctags-5.8-1.src.rpm
# OpenGrok
RUN mkdir /var/opengrok 
RUN wget "https://github.com/OpenGrok/OpenGrok/files/467358/opengrok-0.12.1.6.tar.gz.zip" -O /opt/opengrok-0.12.1.6.tar.gz.zip
RUN unzip /opt/opengrok-0.12.1.6.tar.gz.zip -d /opt
RUN tar zxvf /opt/opengrok-0.12.1.6.tar.gz -C /var/opengrok --strip-components=1
RUN rm /opt/opengrok-0.12.1.6.tar.gz

RUN mkdir -p /var/opengrok/{src,data,etc}
RUN /var/opengrok/bin/OpenGrok deploy

ADD runGrok.sh /opt/runGrok.sh
RUN chmod +x /opt/runGrok.sh
ENTRYPOINT ["/opt/runGrok.sh"]


RUN chown -R 1001:0 /tomcat && chown -R 1001:0 $HOME && chown -R 1001:0 /var/opengrok && \
    chmod -R ug+rw /tomcat &&  chmod -R ug+rw /var/opengrok
    


USER 1001

CMD /opt/runGrok.sh